# go-firestore

## How TO
1. dnf install golang-go
2. mkdir $GOPATH/src/gitlab.com/kentanggoreng
3. cd $GOPATH/src/gitlab.com/kentanggoreng
4. git clone https://gitlab.com/kentanggoreng/quip-server.git
5. go get quip-server
6. cd quip-server
7. go run graphiql.go
8. localhost:8080



![alt text](https://github.com/aeramu/go-firestore/blob/master/project_diagram.png?raw=true)

